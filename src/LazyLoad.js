import picturefill from 'picturefill';
import Modernizr from 'modernizr';

/**
 * Handle lazyloading of images
 *
 * - Set srcset on render
 * - Reevaluate picturefill for IE
 */
export default class LazyLoad {
  /**
   * Construct new object from scope
   */
  constructor(elem, dynamic) {
    // Get scope
    const scope = !elem ? document.body : elem;

    // Get images
    this.images = scope.getElementsByTagName('img');
    this.dynamic = dynamic;
  }

  /**
   * Run image evaluation
   */
  evaluate() {
    this.setSrc();
  }

  /**
   * Set the srcset from the data-srcset value
   */
  setSrc() {
    const { length } = this.images;
    let i = 0;

    for (i; i < length; i += 1) {
      const img = this.images[i];
      const srcset = img.getAttribute('data-srcset');

      if (srcset) {
        img.removeAttribute('srcset');

        // Only bind to onload event if content is dynamic
        if (this.dynamic) {
          img.onload = () => {
            // Remove onload event, to prevent infinite loop
            img.onload = null;

            LazyLoad.setSingleSrc(img, srcset);
          };
        } else {
          LazyLoad.setSingleSrc(img, srcset);
        }

        // If browser doesn't support object-fit, set img as background image
        if (!Modernizr.objectfit) {
          const parent = img.parentNode;

          if (parent.hasAttribute('data-bg')) {
            const src = srcset.split(', ');

            const bigImg = src.pop();
            const bigImgSplit = bigImg.split(' ');
            const bigImgUrl = bigImgSplit[0];

            parent.setAttribute('style', `background-image:url('${bigImgUrl}'); background-size:cover; background-position: center;`);
            img.style.opacity = 0;
          }
        }
      }
    }
  }

  /**
   * Set the srcset correctly for a single img
   */
  static setSingleSrc(img, srcset) {
    img.setAttribute('srcset', srcset);
    img.removeAttribute('data-srcset');

    picturefill({
      reevaluate: true,
      elements: [img],
    });
  }
}
