import LazyLoad from './LazyLoad';

const lazyLoadSrcSet = (scope, dynamic = false) => {
  const lazyLoad = new LazyLoad(scope, dynamic);

  lazyLoad.evaluate();
};

export default lazyLoadSrcSet;
