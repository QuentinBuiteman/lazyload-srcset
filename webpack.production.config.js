// Imports
const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

// Paths
const src = path.resolve(__dirname, 'src');
const dist = path.resolve(__dirname, 'dist');

module.exports = {
  mode: 'production',

  // Entry of our app
  entry: `${src}/index.js`,

  // Output path and filename
  output: {
    path: dist,
    filename: 'index.js',
    library: 'LazyLoadSrcSet',
    libraryTarget: 'umd',
  },

  // Add modules
  module: {
    rules: [
      {
        test: /\.modernizrrc$/,
        use: [
          { loader: 'modernizr-loader' },
          { loader: 'json-loader' }
        ]
      },
      {
        test: /\.js$/,
        include: src,
        loader: 'babel-loader',
      },
    ],
  },

  // Don't minimize
  optimization: {
    minimize: false
  },

  // Standard part for img imports
  resolve: {
    modules: [
      path.resolve(__dirname),
      'node_modules',
    ],
    alias: {
      modernizr$: path.resolve(__dirname, '.modernizrrc'),
    },
  },

  stats: {
    children: false,
  },

  plugins: [
    new ESLintPlugin({
      extensions: ['.js', '.jsx'],
    }),
  ]
};
